<?php

declare(strict_types=1);

namespace Drupal\samples\Access;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\samples\EntityDecorator\Node;

/**
 * Generates node access records and grants access for sample content.
 */
final class NodeAccess {

  /**
   * The site's roles.
   *
   * @var \Drupal\user\RoleInterface[]
   */
  protected array $roles;

  /**
   * The site's node bundle IDs.
   *
   * @var string[]
   */
  protected array $bundles;

  /**
   * Constructs a new instance of this class.
   *
   * @param \Drupal\user\RoleInterface[] $roles
   *   The site's roles.
   * @param string[] $bundles
   *   The site's node bundle IDs.
   */
  public function __construct(array $roles, array $bundles) {
    $this->roles = $roles;
    $this->bundles = $bundles;
  }

  /**
   * Creates a new instance of this class.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity_type.bundle.info service.
   *
   * @return self
   *   A new instance of this class.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function create(EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info): self {
    return new self(
      $entity_type_manager->getStorage('user_role')->loadMultiple(),
      array_keys($entity_type_bundle_info->getBundleInfo('node')),
    );
  }

  /**
   * Generates node access records for a sample content node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node for which access records should be generated, if at all.
   *
   * @return array|null
   *   An array of access records or NULL if the node is not marked as sample
   *   content.
   *
   * @see \samples_node_access_records()
   */
  public function getAccessRecords(NodeInterface $node): ?array {
    $records = [];
    $type_id = $node->bundle();
    foreach ($this->roles as $role) {
      $is_content_admin = $role->isAdmin() || $role->hasPermission('administer nodes');
      $record = [
        'realm' => "content_samples_{$role->id()}_access",
        'gid' => 1,
        'grant_view' => (int) ($is_content_admin || $node->isPublished() && $role->hasPermission('access content')),
        'grant_update' => (int) ($is_content_admin || $role->hasPermission("edit any {$type_id} content")),
        'grant_delete' => (int) ($is_content_admin || $role->hasPermission("delete any {$type_id} content")),
      ];
      if (Node::isSample($node)) {
        $record = $this->withSampleControls($record, $role);
      }
      $records[] = $record;
    }
    if ($node->getOwnerId()) {
      $owner = $node->getOwner();
      $record = [
        'realm' => "own_{$type_id}_content_samples",
        'gid' => (int) $node->getOwnerId(),
        'grant_view' => (int) (!$node->isPublished() && $owner->hasPermission('view own unpublished content')),
        'grant_update' => (int) $owner->hasPermission("edit own {$type_id} content"),
        'grant_delete' => (int) $owner->hasPermission("delete own {$type_id} content"),
      ];
      if (Node::isSample($node)) {
        $record = $this->withSampleControls($record, $owner);
      }
      $records[] = $record;
    }
    return $records;
  }

  /**
   * Adds sample content permission requirements to a node access record.
   *
   * @param array $record
   *   The original record.
   * @param \Drupal\user\RoleInterface|\Drupal\Core\Session\AccountInterface $permission_set
   *   An account or role which has permissions.
   *
   * @return array
   *   The amended node access record.
   */
  protected function withSampleControls(array $record, $permission_set): array {
    assert(method_exists($permission_set, 'hasPermission'));
    // Only grant access if access is *already* granted and the given role or
    // user has the required permissions.
    $record['grant_view'] = (int) ($record['grant_view'] && $permission_set->hasPermission('administer content samples') || $permission_set->hasPermission('access content samples'));
    $record['grant_update'] = (int) ($record['grant_update'] && $permission_set->hasPermission('administer content samples'));
    $record['grant_delete'] = (int) ($record['grant_delete'] && $permission_set->hasPermission('administer content samples'));
    return $record;
  }

  /**
   * Generates node grants for a given user account and node operation.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account for which grants should be generated, if at all.
   * @param string $op
   *   The node operation.
   *
   * @return array
   *   An array of node grants.
   *
   * @see \samples_node_grants()
   */
  public function getGrants(AccountInterface $account, string $op): array {
    if ($op === 'view' && $account->hasPermission('administer nodes') || $account->hasPermission('access content')) {
      $grants['samples_default'][] = 0;
    }
    foreach ($account->getRoles() as $role) {
      $grants["content_samples_{$role}_access"][] = 1;
    }
    foreach ($this->bundles as $type_id) {
      $grants["own_{$type_id}_content_samples"][] = $account->id();
    }
    return $grants;
  }

}
