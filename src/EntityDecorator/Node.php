<?php

declare(strict_types=1);

namespace Drupal\samples\EntityDecorator;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\node\NodeInterface;

/**
 * Generates node access records and grants access for sample content.
 */
final class Node {

  /**
   * Determines if the given node is a sample or not.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to inspect.
   *
   * @return bool
   *   TRUE of the node is a sample; FALSE otherwise.
   */
  public static function isSample(NodeInterface $node): bool {
    $samples_status = $node->get('samples_status');
    return !$samples_status->isEmpty() && $samples_status->value;
  }

  /**
   * Creates a base field definition for storing whether a node is a sample.
   *
   * This field is used to determine if access records should be generated or
   * not.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The field definition.
   *
   * @see \samples_entity_base_field_info()
   */
  public static function getSamplesStatusBaseFieldDefinition(): BaseFieldDefinition {
    return BaseFieldDefinition::create('boolean')
      ->setLabel(t('Sample'))
      ->setDescription(t('Restricts this content to users with the ability to view or manage sample content'))
      ->setDefaultValue(FALSE)
      ->setCardinality(1)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
      ])
      ->setSetting('on_label', t('Use as a sample'));
  }

}
