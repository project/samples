# Sample content

## Overview

Sample content makes development easier through the use of "sample" content.
Samples behave exactly like regular
content, with the exception that they are restricted to users with permission to
view and/or manage those samples.

## Installation

1. [Install the module with Composer](https://www.drupal.org/docs/extending-drupal/installing-modules#s-add-a-module-with-composer)
2. Once the module is in place, visit `/admin/modules` to install the module or
   install it with [Drush](https://www.drush.org).
3. **If your site has pre-existing content**, you **must**
   visit `/admin/reports/status` and follow the link
   titled _Rebuild permissions_ within the _Node Access Permissions_ section.
   This module will not work correctly until
   you have done so. This step is not required if you have installed the module
   prior to any content creation.
4. Finally, grant the _Access content samples_ and _Administer sample content_
   permissions to the appropriate user roles
   for your use case by visiting `/admin/people/permissions/module/samples`.

## Usage

Once the module has been installed and configured, you can create new samples in
the following way:

1. Follow links in the administrative menu to add new content to your site.
2. Once on the content creation form, fill it in with sample values that are
   representative of the kind of content you
   would like to preview and work with.
3. Select the _Use as a sample_ option (found in the administrative section of
   the content form)
4. Save the content.

The _Use as a sample_ option can be found on the edit form for existing content
as well. Once this option has been
enabled, the sample will only be accessible to those with the _Access content
samples_ permission.

Unlike many developer tools, content samples are not meant to be version
controlled or deployed "up" through
development, test, and production environments. They should be created on the "
highest" available environment and
brought "down" like other user-generated data.
