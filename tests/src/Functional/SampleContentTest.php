<?php

declare(strict_types=1);

namespace Drupal\Tests\samples\Functional;

use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\UserInterface;

/**
 * Test the general behavior of sample content.
 *
 * @group samples
 */
class SampleContentTest extends BrowserTestBase {

  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['samples'];

  /**
   * A user that can view regular content, but not samples.
   *
   * @var \Drupal\user\UserInterface
   */
  private UserInterface $authenticatedUser;

  /**
   * A user that can create and edit regular content, but not samples.
   *
   * @var \Drupal\user\UserInterface
   */
  private UserInterface $contentEditor;

  /**
   * A user that can view regular content and samples, but can't edit them.
   *
   * @var \Drupal\user\UserInterface
   */
  private UserInterface $sampleViewer;

  /**
   * A user that can create and edit regular and sample content.
   *
   * @var \Drupal\user\UserInterface
   */
  private UserInterface $sampleAdmin;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    NodeType::create(['type' => 'page', 'name' => 'Basic page'])->save();
    $this->drupalCreateContentType();
    $this->authenticatedUser = $this->createUser(['access content']);
    $this->contentEditor = $this->createUser([
      'access content',
      'access content samples',
      'edit any page content',
    ]);
    $this->sampleViewer = $this->createUser([
      'access content',
      'access content samples',
    ]);
    $this->sampleAdmin = $this->createUser([
      'administer nodes',
      'administer content samples',
    ]);
  }

  /**
   * Tests content sample administration and access permissions.
   */
  public function testSampleContentDesignation(): void {
    $this->drupalLogin($this->contentEditor);
    $node = $this->drupalCreateNode();
    $canonical_url = $node->toUrl();
    // The content editor should be able to view their own content.
    $this->drupalGet($canonical_url);
    $this->assertSession()->titleEquals($node->getTitle() . ' | Drupal');
    // The content editor should not be able to designate the content as a
    // sample.
    $edit_form_url = $node->toUrl('edit-form');
    $this->drupalGet($edit_form_url);
    $this->assertSession()->pageTextNotContains('Use as a sample');
    // The content editor should be able to edit their own content.
    $new_title = $this->randomMachineName(32);
    $this->submitForm([
      'title[0][value]' => $new_title,
    ], 'Save');
    $this->drupalGet($canonical_url);
    $this->assertSession()->titleEquals($new_title . ' | Drupal');
    // Authenticated users should be able to view the content since it is not a
    // sample.
    $this->drupalLogin($this->authenticatedUser);
    $this->drupalGet($canonical_url);
    $this->assertSession()->titleEquals($new_title . ' | Drupal');
    // Authenticated users should not be able to edit content.
    $this->drupalGet($edit_form_url);
    $this->assertSession()->titleEquals('Access denied | Drupal');
    // Sample administrators should be able to designate content samples.
    $this->drupalLogin($this->sampleAdmin);
    $this->drupalGet($edit_form_url);
    $this->assertSession()->pageTextContains('Use as a sample');
    $this->submitForm([
      'samples_status[value]' => '1',
    ], 'Save');
    // And they should still be able to view it afterward.
    $this->assertSession()->titleEquals($new_title . ' | Drupal');
    // Authenticated users should not be able to view samples.
    $this->drupalLogin($this->authenticatedUser);
    $this->drupalGet($canonical_url);
    $this->assertSession()->titleEquals('Access denied | Drupal');
    // Sample viewers should be able to view the sample, but not edit it.
    $this->drupalLogin($this->sampleViewer);
    $this->drupalGet($canonical_url);
    $this->assertSession()->titleEquals($new_title . ' | Drupal');
    $this->drupalGet($edit_form_url);
    $this->assertSession()->titleEquals('Access denied | Drupal');
    // Anonymous visitors should not be able to view or edit samples.
    $this->drupalLogout();
    $this->drupalGet($canonical_url);
    $this->assertSession()->titleEquals('Access denied | Drupal');
    $this->drupalGet($edit_form_url);
    $this->assertSession()->titleEquals('Access denied | Drupal');
  }

}
